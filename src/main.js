import Vue from 'vue'

var config = require('./config');

window.axios = require('axios');
window.axios.defaults.baseURL = config.apiUrl;

window.axios.interceptors.response.use(undefined, (error) => {
  if (error.response && error.response.status === 401) {
    window.location.href = '/auth';
    localStorage.access_token = '';
  }
  return Promise.reject(error);
});

/*Стороние компоненты*/
import Router from 'vue-router'
Vue.use(Router);

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);

import {ClientTable, ServerTable, Event} from 'vue-tables-2';
Vue.use(ClientTable);
Vue.use(ServerTable);

import VModal from 'vue-js-modal';
Vue.use(VModal);

import vueKanban from 'vue-kanban';
Vue.use(vueKanban);

import Vuex from 'vuex';
Vue.use(Vuex);

import Toasted from 'vue-toasted';
Vue.use(Toasted, {duration: 1500});

import {store} from './store';

/*Мои компонетны*/
import App from './App.vue';
import Post from './Post.vue';
import About from './components/About.vue';
import Table from './components/VueTable.vue';
import Todo from "./components/Todo";
import Index from "./components/Index";
import Form from "./components/Form";

/*Страница авторизации*/
import AuthPage from "./components/Auth/AuthPage";

/*Маршруты*/
const router = new Router({
    mode: 'history',
    routes: [
        {path: '/', name: 'home', component: Index,},
        {path: '/post/:id', name: 'post', component: Post, props: true,},
        {path: '/about', name: 'about', component: About,},
        {path: '/table', name: 'table', component: Table,},
        {path: '/todo', name: 'todo', component: Todo,},
        {path: '/form', name: 'form', component: Form,},

        /*Auth*/
        {path: '/auth', name: 'auth', component: AuthPage,},
    ],
});

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
