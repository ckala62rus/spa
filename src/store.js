import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({

  state: {
    auth_data: localStorage.getItem('access_token') || null,
  },

  getters: {

    AUTH_DATA: state => {
      return state.auth_data;
    },

    DELETE_AUTH_DATA: (state, payload) => {
      state.auth_data = null;
      window.location.href = config.apiUrl || '/login';
    },

  },

  mutations: {

    SET_AUTH_DATA: (state, payload) => {
      state.auth_data = payload;
    },

  },

  actions: {

    SAVE_AUTH_DATA: (context, payload) => {
      localStorage.setItem('access_token', payload.access_token);
      context.commit('SET_AUTH_DATA', payload.access_token);
      window.location.href = '/';
    },

  },

});
